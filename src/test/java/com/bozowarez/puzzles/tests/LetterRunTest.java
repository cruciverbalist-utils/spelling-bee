package com.bozowarez.puzzles.tests;

import com.bozowarez.puzzles.LetterRun;
import org.junit.Assert;
import org.junit.Test;

import java.util.HashSet;

public class LetterRunTest {
    @Test(expected = IllegalArgumentException.class)
    public void Constructor_NullSource_ExceptionThrown() {
        LetterRun letterRun = new LetterRun(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void Constructor_NonLettersInSource_ExceptionThrown() {
        LetterRun letterRun = new LetterRun("okey-doke");
    }

    @Test
    public void Constructor_LowercaseLettersInSource_ConvertedToUpperCase() {
        LetterRun letterRun = new LetterRun("Hello");
        Assert.assertEquals("HELLO", letterRun.getContent());
    }

    @Test
    public void Constructor_LeadingOrTrailingSpacesInSource_Ignored() {
        LetterRun letterRun = new LetterRun("  hello  ");
        Assert.assertEquals("HELLO", letterRun.getContent());
    }

    @Test
    public void ContentReflectsSource() {
        LetterRun letterRun = new LetterRun("TESTING");
        Assert.assertEquals("TESTING", letterRun.getContent());
    }

    @Test
    public void LetterSetContainsExpectedLetters() {
        LetterRun letterRun = new LetterRun("WORLD");
        HashSet<Character> set = letterRun.getLetterSet();
        Assert.assertEquals(5, set.size());
        Assert.assertTrue(set.contains('W'));
        Assert.assertTrue(set.contains('O'));
        Assert.assertTrue(set.contains('R'));
        Assert.assertTrue(set.contains('L'));
        Assert.assertTrue(set.contains('D'));

        letterRun = new LetterRun("ACACIA");
        set = letterRun.getLetterSet();
        Assert.assertEquals(3, set.size());
        Assert.assertTrue(set.contains('A'));
        Assert.assertTrue(set.contains('C'));
        Assert.assertTrue(set.contains('I'));

        letterRun = new LetterRun("BOOKKEEPER");
        set = letterRun.getLetterSet();
        Assert.assertEquals(6, set.size());
        Assert.assertTrue(set.contains('B'));
        Assert.assertTrue(set.contains('O'));
        Assert.assertTrue(set.contains('K'));
        Assert.assertTrue(set.contains('E'));
        Assert.assertTrue(set.contains('P'));
        Assert.assertTrue(set.contains('R'));
    }

    @Test
    public void UniqueLetterCountCalculatedCorrectly() {
        LetterRun letterRun = new LetterRun("SIMPLE");
        int uniqueLetterCount = letterRun.getUniqueLetterCount();
        Assert.assertEquals(6, uniqueLetterCount);

        letterRun = new LetterRun("TESTING");
        uniqueLetterCount = letterRun.getUniqueLetterCount();
        Assert.assertEquals(6, uniqueLetterCount);

        letterRun = new LetterRun("COMMITTEES");
        uniqueLetterCount = letterRun.getUniqueLetterCount();
        Assert.assertEquals(7, uniqueLetterCount);
    }

    @Test
    public void SlugReflectsAlphabetizedLetterSet() {
        LetterRun letterRun = new LetterRun("WORLD");
        String slug = letterRun.getSlug();
        Assert.assertEquals("DLORW", slug);

        letterRun = new LetterRun("ACACIA");
        slug = letterRun.getSlug();
        Assert.assertEquals("ACI", slug);

        letterRun = new LetterRun("BOOKKEEPER");
        slug = letterRun.getSlug();
        Assert.assertEquals("BEKOPR", slug);
    }
}
