package com.bozowarez.puzzles;

public class SpellingBee {
    private LetterComb letterComb;
    private int threePointWordCount;
    private int maxScore;

    public SpellingBee(LetterComb letterComb, int threePointWordCount, int maxScore) {
        if (letterComb == null) throw new IllegalArgumentException("letterComb cannot be null");

        this.letterComb = letterComb;
        this.threePointWordCount = threePointWordCount;
        this.maxScore = maxScore;
    }

    public LetterComb getLetterComb() {
        return letterComb;
    }

    public int getThreePointWordCount() {
        return threePointWordCount;
    }

    public int getMaxScore() {
        return maxScore;
    }
}
