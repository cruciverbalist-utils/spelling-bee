package com.bozowarez.puzzles;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class SpellingBeeCreator {
    private WordList wordList;

    public SpellingBeeCreator(WordList wordList) {
        if (wordList == null) throw new IllegalArgumentException("wordList cannot be null");
        this.wordList = wordList;
    }

    public SpellingBee create() {
        List<LetterRun> targetWords = new ArrayList<LetterRun>();
        for (LetterRun letterRun : wordList.getWords()) {
            if (letterRun.getUniqueLetterCount() == 7) {
                targetWords.add(letterRun);
            }
        }

        Random random = new Random();
        int index = random.nextInt(targetWords.size());
        LetterRun targetWord = targetWords.get(index);
        LetterComb letterComb = LetterComb.CreateFromTargetWord(targetWord);

        SpellingBeeSolver solver = new SpellingBeeSolver(wordList);
        SpellingBeeSolution solution = solver.Solve(letterComb);

        return new SpellingBee(letterComb, solution.getThreePointWordCount(), solution.getScore());
    }
}
