package com.bozowarez.puzzles;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

public class SpellingBeeSolution {
    private LetterComb letterComb;
    private HashMap<String, Integer> wordScores = new HashMap<>();
    
    public SpellingBeeSolution(LetterComb letterComb) {
        this.letterComb = letterComb;
    }
    
    public void addWord(LetterRun letterRun, int score) {
        wordScores.put(letterRun.getContent(), score);
    }

    public String getComb() {
        return letterComb.toString();
    }

    public List<String> getWords() {
        List<String> onePointers = new ArrayList<>();
        List<String> threePointers = new ArrayList<>();

        for (HashMap.Entry<String, Integer> entry : wordScores.entrySet()) {
            String word = entry.getKey();
            int score = entry.getValue();
            if (score == 1) {
                onePointers.add(word);
            }
            else {
                threePointers.add(word);
            }
        }

        onePointers.sort(Comparator.naturalOrder());
        threePointers.sort(Comparator.naturalOrder());

        List<String> result = new ArrayList<>();
        result.addAll(threePointers);
        result.addAll(onePointers);
        return result;
    }

    public int getScore() {
        int total = 0;

        for (HashMap.Entry<String, Integer> entry : wordScores.entrySet()) {
            total += entry.getValue();
        }

        return total;
    }

    public int getThreePointWordCount() {
        int total = 0;
        for (HashMap.Entry<String, Integer> entry : wordScores.entrySet()) {
            int score = entry.getValue();
            if (score == 3) {
                total++;
            }
        }

        return total;
    }
}
