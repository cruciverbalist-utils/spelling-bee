package com.bozowarez.puzzles;

public class SpellingBeeSolver {
    private WordList wordList;

    public SpellingBeeSolver(WordList wordList) {
        if (wordList == null) throw new IllegalArgumentException("wordList cannot be null");
        this.wordList = wordList;
    }

    public SpellingBeeSolution Solve(LetterComb comb) {
        SpellingBeeSolution solution = new SpellingBeeSolution(comb);

        for (LetterRun word : wordList.getWords()) {
            int score = comb.Score(word);
            if (score > 0) {
                solution.addWord(word, score);
            }
        }

        return solution;
    }
}
