package com.bozowarez.puzzles;

import java.util.HashSet;
import java.util.Random;
import java.util.stream.Collectors;

public class LetterComb {
    private char requiredLetter;
    private HashSet<Character> letters;

    public static LetterComb CreateFromNewspaper(String source) throws IllegalArgumentException {
        LetterRun letterRun = new LetterRun(source);

        if (letterRun.getLength() != 7) {
            throw new IllegalArgumentException("Source string must be seven letters long");
        }

        if (letterRun.getLetterSet().size() != 7) {
            throw new IllegalArgumentException("Source string must have exactly seven unique letters");
        }

        char requiredLetter = letterRun.getContent().charAt(3);
        return new LetterComb(requiredLetter, new HashSet<>(letterRun.getLetterSet()));
    }

    public static LetterComb CreateFromTargetWord(LetterRun targetWord) {
        if (targetWord.getLength() < 7) {
            throw new IllegalArgumentException("Target word must be at least seven letters long");
        }

        if (targetWord.getLetterSet().size() != 7) {
            throw new IllegalArgumentException("Target word must have exactly seven unique letters");
        }

        Random rand = new Random();
        String slug = targetWord.getSlug();
        int requiredLetterIndex = rand.nextInt(slug.length());
        char requiredLetter = slug.charAt(requiredLetterIndex);

        return new LetterComb(requiredLetter, new HashSet<>(targetWord.getLetterSet()));
    }

    private LetterComb(char requiredLetter, HashSet<Character> letters) {
        this.requiredLetter = requiredLetter;
        this.letters = letters;
    }

    private Boolean Check(LetterRun word) {
        if (word == null) return false;

        if (!word.getLetterSet().contains(this.requiredLetter)) {
            return false;
        }

        for (int i = 0; i < word.getSlug().length(); i++) {
            char ch = word.getSlug().charAt(i);
            if (!letters.contains(ch)) {
                return false;
            }
        }

        return true;
    }

    public int Score(LetterRun word) {
        if (!Check(word)) return 0;
        return word.getUniqueLetterCount() == 7 ? 3 : 1;
    }

    @Override
    public String toString() {
        HashSet<Character> extraLetterSet = new HashSet<Character>();
        extraLetterSet.addAll(letters);
        extraLetterSet.remove(requiredLetter);

        char[] extraLetters = extraLetterSet.stream()
                .sorted()
                .map(String::valueOf)
                .collect(Collectors.joining()).toCharArray();

        StringBuilder sb = new StringBuilder();
        sb.append(' ');
        sb.append(extraLetters[0]);
        sb.append(' ');
        sb.append(extraLetters[1]);
        sb.append('\n');
        sb.append(extraLetters[2]);
        sb.append(' ');
        sb.append(requiredLetter);
        sb.append(' ');
        sb.append(extraLetters[3]);
        sb.append('\n');
        sb.append(' ');
        sb.append(extraLetters[4]);
        sb.append(' ');
        sb.append(extraLetters[5]);
        sb.append('\n');
        return sb.toString();
    }
}
