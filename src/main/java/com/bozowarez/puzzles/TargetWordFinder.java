package com.bozowarez.puzzles;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

public class TargetWordFinder {
    public static List<String> findTargets(List<String> candidates) {
        List<String> output = new ArrayList<>();
        for (String candidate : candidates) {
            if (hasExactlySevenUniqueLetters(candidate)) {
                output.add(candidate);
            }
        }
        return output;
    }

    private static Boolean hasExactlySevenUniqueLetters(String candidate)
    {
        if (candidate.length() < 7) {
            return false;
        }

        HashSet<Character> letters = new HashSet<>();
        for (int i = 0; i < candidate.length(); i++) {
            char ch = candidate.charAt(i);
            letters.add(ch);
        }

        return letters.size() == 7;
    }
}
