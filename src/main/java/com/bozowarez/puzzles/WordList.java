package com.bozowarez.puzzles;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class WordList {
    private List<LetterRun> words = new ArrayList<>();

    public WordList(String filename) throws IOException {
        List<String> lines = Files.readAllLines(Paths.get(filename));

        for (String line: lines) {
            try {
                LetterRun word = new LetterRun(line);
                words.add(word);
            }
            catch (IllegalArgumentException e) {
                // do nothing
            }
        }
    }

    public List<LetterRun> getWords() {
        return words;
    }
}
