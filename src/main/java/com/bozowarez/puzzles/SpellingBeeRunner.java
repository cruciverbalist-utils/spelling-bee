package com.bozowarez.puzzles;

import java.io.IOException;

public class SpellingBeeRunner {
    public static void main(String[] args) {
        solve(args);
    }

    private static void create(String[] args) {
        if (args.length < 1) {
            System.out.println("No word list file provided.");
            System.exit(1);
        }

        String filename = args[0];
        try {
            WordList wordList = new WordList(filename);

            SpellingBeeCreator creator = new SpellingBeeCreator(wordList);
            SpellingBee spellingBee = creator.create();

            System.out.println(spellingBee.getLetterComb());
            System.out.println("Number of three-point words: " + spellingBee.getThreePointWordCount());
            System.out.println("Maximum score: " + spellingBee.getMaxScore());
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(2);
        }

    }

    private static void solve(String[] args) {
        if (args.length < 1) {
            System.out.println("No word list file provided.");
            System.exit(1);
        }

        String filename = args[0];
        try {
            WordList wordList = new WordList(filename);

            SpellingBeeSolver solver = new SpellingBeeSolver(wordList);
            LetterComb comb = LetterComb.CreateFromNewspaper("ENPOSUX");
            SpellingBeeSolution solution = solver.Solve(comb);

            System.out.println(comb);

            System.out.println("Total score: " + solution.getScore());
            for (String result : solution.getWords())
            {
                System.out.println(result);
            }
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(2);
        }
    }
}
