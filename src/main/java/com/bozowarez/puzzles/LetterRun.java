package com.bozowarez.puzzles;

import java.util.HashSet;
import java.util.stream.Collectors;

public class LetterRun {
    private String content;
    private HashSet<Character> letterSet = null;
    private String slug = null;

    public LetterRun(String source) throws IllegalArgumentException {
        if (source == null) {
            throw new IllegalArgumentException("Source string cannot be null");
        }

        content = source.trim().toUpperCase();
        if (!content.matches("[A-Z]+")) {
            throw new IllegalArgumentException("Source string must only contain letters.");
        }

    }

    public String getContent() {
        return content;
    }

    public int getLength() {
        return content.length();
    }

    public HashSet<Character> getLetterSet() {
        if (letterSet == null) {
            letterSet = new HashSet<>();
            for (int i = 0; i < content.length(); i++) {
                char ch = content.charAt(i);
                letterSet.add(ch);
            }
        }

        return letterSet;
    }

    public int getUniqueLetterCount() {
        HashSet<Character> set = getLetterSet();
        return set.size();
    }

    public String getSlug() {
        if (slug == null) {
            slug = getLetterSet().stream()
                                 .sorted()
                                 .map(String::valueOf)
                                 .collect(Collectors.joining());
        }

        return slug;
    }

    @Override
    public String toString() {
        return content;
    }
}
